//2 Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
		{
			$match: { onSale: true }},
		{
			$count: "fruitsonSale"}
	])

//3 Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
		{
			$match: { stock: {$gte: 20}}},
		{
			$count: "plentyStocks"}
	])

//4 Use the average operator to get the average price of fruitsonSale per supplier
db.fruits.aggregate([
		{
			$match: { onSale: true }},
		{
			$group: { _id: null, avg_pice: { $avg: "$price" }}
		}
	])

//5 Use the Max Operator to get the highest price of a fruit per supplier
db.fruits.aggregate([
		{
			$match: { onSale: true }},
		{
			$group: { _id: null, max_price: { $max: "$price" }}}
	])

//6 Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
		{
			$match: { onSale: true }},
		{
			$group: { _id: null, min_price: { $min: "$price" }}}
	])
